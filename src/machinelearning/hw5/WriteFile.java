package machinelearning.hw5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

public class WriteFile {

	public WriteFile() {
	}

	public void write(File outputFile, String text) {
		try {
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(
							outputFile.getAbsoluteFile()), "UTF-8"));
			bw.write(text + "\n");
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeByMap(File outputFile,
			Map<String, Map<String, Integer>> extractInCategory) {
		try {
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(
							outputFile.getAbsoluteFile()), "UTF-8"));
			for (String category : extractInCategory.keySet()) {
				bw.write(category + "\n");
				bw.write(extractInCategory.get(category) + "\n");
			}
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
