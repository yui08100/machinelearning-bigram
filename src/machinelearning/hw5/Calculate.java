package machinelearning.hw5;

import java.util.LinkedList;
import java.util.Map;

public class Calculate {

	// public Calculate() {
	// }

	public static double calculateIG(Map<String, Integer> value) {
		double calculateValue = 0;
		for (String term : value.keySet()) {
			calculateValue = calculateValue + ((double)value.get(term) * log((double)value.get(term), 2));
		}
		return calculateValue;
	}

	public static double log(double value, double base) {
		if (value == 0) {
			return 0;
		} else {
			return Math.log(value) / Math.log(base);
		}
	}
}
