package machinelearning.hw5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		try {
			WriteFile wf = new WriteFile();
			/*第一題*/
			Bigram bg = new Bigram(new File("hw5-train.txt"));
			Set<String> all_term =  bg.extractBigrams();
			wf.write(new File("bigram.txt"), all_term.toString());
			/*第二題*/
			bg = new Bigram(new File("hw5-train.txt"));
			wf= new WriteFile();
			Map<String, Map<String, Integer>> term_frequency = bg.extractInCategory();
			wf.writeByMap(new File("term_frequency.txt"), term_frequency);
			/*第三題*/
			InformationGain ig = new InformationGain(term_frequency, all_term, new File("hw5-train.txt"));
			ig.extractFile();
			
		} catch (FileNotFoundException e) {
			System.err.print("hw5-train.txt not found");
			e.printStackTrace();
		}

	}

}
