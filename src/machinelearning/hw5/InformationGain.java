package machinelearning.hw5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class InformationGain {

//	private Map<String, Map<String, Integer>> terms_frequency;
	private Map<String, Map<String, Integer>> terms_in_category_files;
//	private Set<String> all_terms;
	private Map<String, Integer> terms_in_files;
	private int file_count;
	private Map<String, Integer> label_count;
	private InputStream input_file;

	public InformationGain(Map<String, Map<String, Integer>> term_frequency, Set<String> all_term, File inputFile) throws FileNotFoundException {
		this.input_file = new FileInputStream(inputFile);
		this.terms_in_category_files = new HashMap<String, Map<String, Integer>>();
		for (String label : term_frequency.keySet()) {
			Map<String, Integer> terms = new HashMap<String, Integer>();
			for (String term : term_frequency.get(label).keySet()) {
				terms.put(term, 0);
			}
			this.terms_in_category_files.put(label, terms);
			terms.clear();
		}
		this.terms_in_files = new HashMap<String, Integer>();
		for (String term : all_term) {
			this.terms_in_files.put(term, 0);
		}
//		this.all_terms = all_term;
		this.terms_in_files = new HashMap<String, Integer>();
		this.file_count = 0;
		this.label_count = new HashMap<String, Integer>();
//		System.out.println(this.terms_in_category_files);
//		System.out.println(this.terms_in_files);
//		System.out.println("dddd "+term_frequency);
	}
	
	public void extractFile() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				this.input_file, "UTF-8"))) {
			String line = br.readLine();
			while (line != null) {
				String label = line.substring(1, 3);
				String[] spilt_string = line.split("#");
				this.file_count++;
				if(this.label_count.containsKey(label)) {
					this.label_count.put(label, this.label_count.get(label)+1);
				} else {
					this.label_count.put(label, 1);
				}
				for (String term : this.terms_in_category_files.get(label).keySet()) {
					if(spilt_string[2].contains(term) || spilt_string[3].contains(term) || spilt_string[4].contains(term)) {
						this.terms_in_category_files.get(label).put(term, (this.terms_in_category_files.get(label).get(term)+1));
						this.terms_in_files.put(term, this.terms_in_files.get(term));
					}
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
//		System.out.println(this.terms_in_category_files);
//		System.out.println(this.terms_in_files);
	}
	
	public void calculateAllTermIG() {
		Map<String, Map<String, Double>> terms_IG = new HashMap<String, Map<String, Double>>();
		double term_IG = 0;
		double categorys_IG = (-1) * Calculate.calculateIG(this.label_count);
		for(String label : this.label_count.keySet()) {
			
		}
	}
	
//	private void join

}
