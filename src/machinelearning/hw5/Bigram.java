package machinelearning.hw5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Bigram {

	private FileInputStream input_file;
	private Set<String> punctuation_list;

	public Bigram(File inputFile) throws FileNotFoundException {
		this.input_file = new FileInputStream(inputFile);
		this.punctuation_list = new HashSet<String>();
		String[] Punctuation = { "（", "）", "「", "」", "／", ",", "│", "。", "–",
				"：", "、", "；", "『", "』", "？", ".", "<", ">", "^", ":", "-",
				" ", "，", "?", "‧", "]", "[", "∣", "~", "╱" };
		for (String Punctuation_str : Punctuation) {
			this.punctuation_list.add(Punctuation_str);
		}
	}

	public Set<String> extractBigrams() {
		Set<String> bigram_terms = new HashSet<String>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				this.input_file, "UTF-8"))) {
			String line = br.readLine();
			while (line != null) {
				String[] spilt_string = line.split("#");
				bigram_terms.addAll(getTerms(spilt_string[2]));
				bigram_terms.addAll(getTerms(spilt_string[3]));
				bigram_terms.addAll(getTerms(spilt_string[4]));
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bigram_terms;
	}

	public Map<String, Map<String, Integer>> extractInCategory() {
		Map<String, Map<String, Integer>> bigram_terms = new HashMap<String, Map<String, Integer>>();

		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				this.input_file, "UTF8"))) {
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] spilt_string = line.split("#");
				String label = line.substring(1, 3);
				Map<String, Integer> term_frequency = new HashMap<String, Integer>();
				term_frequency.putAll(getTermsInFrequency(spilt_string[2]));
				merageFrequency(term_frequency, getTermsInFrequency(spilt_string[3]));
				merageFrequency(term_frequency, getTermsInFrequency(spilt_string[4]));

				if (bigram_terms.keySet().contains(label)) {
					merageFrequency(bigram_terms.get(label), term_frequency);
				} else {
					bigram_terms.put(label, term_frequency);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bigram_terms;
	}

	private Set<String> getTerms(String targetString) {
		Set<String> term_set = new HashSet<String>();
		for (int i = 0; i < targetString.length() - 2; i++) {
			String term = targetString.substring(i, i + 2);
			if (this.punctuation_list.contains(term.substring(0, 1))
					|| this.punctuation_list.contains(term.substring(1, 2))) {
				continue;
			}
			term_set.add(term);
		}
		return term_set;
	}

	private Map<String, Integer> getTermsInFrequency(String targetString) {
		Map<String, Integer> term_map = new HashMap<String, Integer>();
		for (int i = 0; i < targetString.length() - 2; i++) {
			String term = targetString.substring(i, i + 2);
			if (this.punctuation_list.contains(term.substring(0, 1))
					|| this.punctuation_list.contains(term.substring(1, 2))) {
				continue;
			}
			if (term_map.containsKey(term)) {
				term_map.put(term, term_map.get(term) + 1);
			} else {
				term_map.put(term, 1);
			}
		}
		return term_map;
	}

	private Map<String, Integer> merageFrequency(Map<String, Integer> all_set,
			Map<String, Integer> term_set) {
		for (String term_key : term_set.keySet()) {
			if (all_set.containsKey(term_key)) {
				all_set.put(term_key,
						all_set.get(term_key) + term_set.get(term_key));
			} else {
				all_set.put(term_key, term_set.get(term_key));
			}
		}
		return all_set;
	}
}
